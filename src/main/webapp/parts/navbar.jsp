<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: VadykMg
  Date: 03.07.2018
  Time: 9:39
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand">TravelBlog</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/">Статті<span class="sr-only">(current)</span></a>
            </li>
            <form class="form-inline my-2 my-lg-0" method="get" action="${pageContext.request.contextPath}/search">
                <input class="form-control mr-sm-2" name="tag" type="search" placeholder="Пошук..." aria-label="Пошук...">
                <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Знайти</button>
            </form>
        </ul>

        <ul class="navbar-nav ml-auto">
            <c:if test="${id == null}">
                <li class="nav-item">
                    <a class="nav-link disabled" href="${pageContext.request.contextPath}/protected/">Увійти<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="${pageContext.request.contextPath}/registration">Реєстрація<span
                            class="sr-only">(current)</span></a>
                </li>
            </c:if>
        </ul>

        <c:if test="${id != null}">
        <div class="dropdown" style="width: 12.5rem">
            <button class="btn btn-primary dropdown-toggle"style="position: relative; float: right" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    ${username}
            </button>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <c:if test="${role == 'USER'}">
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/protected/userEdit?id=${id}">Профайл</a>
                </c:if>
                <a class="dropdown-item" href="${pageContext.request.contextPath}/protected/addArticle?id=${id}">Додати статтю</a>
                <a class="dropdown-item" href="${pageContext.request.contextPath}/protected/userArticles">Мої статті</a>
                <a class="dropdown-item" href="${pageContext.request.contextPath}/protected/userNoArticles">Чернетки</a>
                    <c:if test="${role == 'ADMIN'}">
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/protected/users">Список користувачів</a>
                    </c:if>
                <a class="dropdown-item" href="${pageContext.request.contextPath}/protected/logout">Вийти</a>
            <div>
         </div>
         </c:if>
</nav>
</html>
