<%--
  Created by IntelliJ IDEA.
  User: VadykMg
  Date: 03.07.2018
  Time: 10:37
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="../parts/header.jsp"/>
<body class="text-center">
<jsp:include page="../parts/navbar.jsp"/>
<form method="post" action="${pageContext.request.contextPath}/protected/userEdit" class="form-group">
    <input type="hidden" value="${user.id}" name="id">
    <input type="hidden" value="${role}" name = "role">
    <h1>${user.username}</h1>
    <input type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" placeholder="password" name="password"><br><br>
    <c:if test="${role == 'ADMIN'}">
        <input type="checkbox" name="ADMIN" id="ADMIN" <c:if test="${fn:contains(user.role, 'ADMIN')}">checked</c:if>>
        <label for="ADMIN">ADMIN</label><br>
        <input type="checkbox" name="USER" id="USER" <c:if test="${fn:contains(user.role, 'USER')}">checked</c:if>>
        <label for="USER">USER</label><br>
    </c:if>
    <input class="btn btn-primary" type="submit" value="Зберегти">
</form>
<jsp:include page="../parts/scripts.jsp"/>
</body>
</html>
