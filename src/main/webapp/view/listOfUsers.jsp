<%--
  Created by IntelliJ IDEA.
  User: VadykMg
  Date: 03.07.2018
  Time: 10:05
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="../parts/header.jsp"/>

<body>
<jsp:include page="../parts/navbar.jsp"/>

<c:if test="${role == 'ADMIN'}">

<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col">Username</th>
        <th scope="col">Roles</th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="user" items="${usersList}">
        <tr>
            <td>${user.username}</td>
            <td><c:forEach var="role" items="${user.role}">${role} </c:forEach></td>
            <td>
                <form method="get" action="${pageContext.request.contextPath}/protected/userEdit">
                    <input type="hidden" name="id" value="${user.id}">
                    <input class="btn btn-primary" type="submit" value="Редагувати">
                </form>
            </td>
            <td><c:if test="${!fn:contains(user.role, 'ADMIN')}">
                <form method="post" action="${pageContext.request.contextPath}/protected/userDelete">
                    <input type="hidden" name="id" value="${user.id}">
                    <input class="btn btn-danger" type="submit" value="Видалити">
                </form>
            </c:if></td>
            <c:if test="${user.isBaned == false}">
            <td><c:if test="${!fn:contains(user.role, 'ADMIN')}">
                <form method="post" action="${pageContext.request.contextPath}/protected/ban">
                    <input type="hidden" name="username" value="${user.username}">
                    <input class="btn btn-danger" type="submit" value="Забанити">
                </form>
            </c:if>
                </c:if>
            </td>
                <c:if test="${user.isBaned == true}">
            <td><c:if test="${!fn:contains(user.role, 'ADMIN')}">
                <form method="post" action="${pageContext.request.contextPath}/protected/unBan">
                    <input type="hidden" name="username" value="${user.username}">
                    <input class="btn btn-primary" type="submit" value="Розбанити">
                </form>
            </c:if>
                </c:if>
            </td>
        </tr>
    </c:forEach>
    </tbody>
    </c:if>
</table>
<jsp:include page="../parts/scripts.jsp"/>
</body>
</html>
