<%--
  Created by IntelliJ IDEA.
  User: VadykMg
  Date: 04.07.2018
  Time: 11:51
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="../parts/header.jsp"/>
<body>
<jsp:include page="../parts/navbar.jsp"/>
<div class="container">
    <div class="form-group">
        <form action="${pageContext.request.contextPath}/protected/editArticle" method="post" id="editArticle">
            <input class="form-control" type="hidden" value="${article.id}" name="id" id="id">
            <input class="form-control" type="text" name="name" placeholder="Назва статті" value="${article.name}"
                   id="name">
            <label for="name">Назва Статті</label><br>
            <input class="form-control" type="text" name="description" placeholder="Текст"
                   value="${article.description}" id="description">
            <label for="description">Текст</label><br>
            <textarea class="form-control" rows="12" name="text" form="editArticle"
                      placeholder="Текст статті">${article.text}</textarea><br>
            <input type="checkbox" name="available" id="available"
                   <c:if test="${article.available == true}">checked</c:if>>
            <label for="available">Публікувати</label><br>
            <input class="btn btn-primary" type="submit" value="update"><br>
        </form>
    </div>
</div>
<jsp:include page="../parts/scripts.jsp"/>
</body>
</html>
