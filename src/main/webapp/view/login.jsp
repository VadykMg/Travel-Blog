<%--
  Created by IntelliJ IDEA.
  User: VadykMg
  Date: 02.07.2018
  Time: 18:23
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<jsp:include page="../parts/header.jsp"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/styles/login.css">
<body class="text-center">
<form class="form-signin" method="post" action="">
    <h1 class="h3 mb-3 font-weight-normal">Авторизація</h1>
    <input type="hidden" value="${isExist}" id="isExist" name="isExist">
    <label for="username" class="sr-only">Username</label>
    <input type="text" onchange="checkBan()" name="username" id="username" class="form-control" placeholder="Username" required autofocus>
    <label for="password" class="sr-only">Password</label>
    <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Увійти">
    <a href="<c:url value="/${pageContext.request.contextPath}/registration"/>">Реєстрація</a>
</form>

<jsp:include page="../parts/scripts.jsp"/>
<script>
    function checkBan() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                console.log(this.responseText);
                if(this.responseText == "true")
                    username.setCustomValidity("Вибачте, ви забанені");
                else
                    username.setCustomValidity("");
            }
        };
        xhttp.open("POST", "${pageContext.request.contextPath}/checkBan", true);
        var sendObject = username.value;
        xhttp.send(sendObject);

    }
</script>
</body>
</html>
