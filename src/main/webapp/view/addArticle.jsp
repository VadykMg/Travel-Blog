<%--
  Created by IntelliJ IDEA.
  User: VadykMg
  Date: 03.07.2018
  Time: 13:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="/parts/header.jsp"/>
<body>
<jsp:include page="/parts/navbar.jsp"/>
<div class="container">
    <div class="form-group">
        <form action="${pageContext.request.contextPath}/protected/addArticle" method="post" enctype="multipart/form-data" id="addArticle">
            <input class="form-control" type="hidden" value="${id}" name="id">
            <input class="form-control" type="text" id="name" onchange="chekName()" name="name"
                   placeholder="Назва статті" required><br>
            <input class="form-control" type="text" id="description" onchange="chekDescription()" name="description"
                   placeholder="Текст" required><br>
            <textarea class="form-control" onchange="chekText()" id="text" rows="12" name="text" form="addArticle"
                      placeholder="Текст статті" required></textarea><br>
            <input type="checkbox" name="available" id="available">
            <label for="available">Публікувати</label><br>
            <input class="form-control" id="tag" onchange="chekTag()" type="text" name="tag" required placeholder="Тег"><br>
            <input class="form-control-file" type="file" accept=".jpg,.jpeg,.png" name="image" id="image" multiple="true">
            <input class="btn btn-primary" type="submit" value="add"><br>
        </form>
    </div>
</div>
<jsp:include page="/parts/scripts.jsp"/>

<script>
    var name = document.getElementById("name");
    var description = document.getElementById("description");
    var text = document.getElementById("text");
    var tag = document.getElementById("tag");

    function chekName() {
        if (name.value == "")
            name.setCustomValidity("Назва не може бути порожня");
        else
            name.setCustomValidity("");
    }

    function chekDescription() {
        if (description.value == "")
            description.setCustomValidity("Опис не може бути порожня");
        else
            description.setCustomValidity("");
    }

    function chekText() {
        if (text.value == "")
            text.setCustomValidity("Текст не може бути порожня");
        else
            text.setCustomValidity("");
    }

    function chekTag() {
        if (tag.value == "")
            tag.setCustomValidity("Текст не може бути порожня");
        else
            tag.setCustomValidity("");
    }
</script>
</body>
</html>
