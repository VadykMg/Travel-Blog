<%--
  Created by IntelliJ IDEA.
  User: VadykMg
  Date: 02.07.2018
  Time: 15:38
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="../parts/header.jsp"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/styles/login.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/styles/registration.css">
<body class="text-center">
<form class="form-signin" action="${pageContext.request.contextPath}/registration" method="post">
    <h1 class="h3 mb-3 font-weight-normal">Реєстрація</h1>
    <label for="username" class="sr-only">Username</label>
    <input type="text" onchange="checkUsername()" name="username" id="username" class="form-control"
           placeholder="Username" required autofocus>
    <label for="password" class="sr-only">Password</label>
    <input type="password" class="form-control" name="password" id="password" placeholder="Password"
           pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
           title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
           required>
    <label for="c_password" class="sr-only">Password</label>
    <input type="password" class="form-control" name="c_password" id="c_password" onchange="checkPasswords()"
           placeholder="Confirm password"
           required>
    <input class="btn btn-lg btn-primary btn-block"  type="submit" value="Зареєструватися">
</form>

<div id="message" ц>
    <h3>Password must contain the following:</h3>
    <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
    <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
    <p id="number" class="invalid">A <b>number</b></p>
    <p id="length" class="invalid">Minimum <b>8 characters</b></p>
    <p id="cp" class="invalid"> Passwords must <b>match</b></p>
</div>
<jsp:include page="../parts/scripts.jsp"/>
<script>
    var username = document.getElementById("username");
    var myInput = document.getElementById("password");
    var letter = document.getElementById("letter");
    var capital = document.getElementById("capital");
    var number = document.getElementById("number");
    var length = document.getElementById("length");
    var confirmPassword = document.getElementById("c_password");
    var cp = document.getElementById("cp");
    // When the user clicks on the password field, show the message box
    myInput.onfocus = function () {
        document.getElementById("message").style.display = "block";
    }

    confirmPassword.onfocus = function () {
        document.getElementById("message").style.display = "block";
    }


    // When the user starts to type something inside the password field
    myInput.onkeyup = function () {
        // Validate lowercase letters
        var lowerCaseLetters = /[a-z]/g;
        if (myInput.value.match(lowerCaseLetters)) {
            letter.classList.remove("invalid");
            letter.classList.add("valid");
        } else {
            letter.classList.remove("valid");
            letter.classList.add("invalid");
        }

        // Validate capital letters
        var upperCaseLetters = /[A-Z]/g;
        if (myInput.value.match(upperCaseLetters)) {
            capital.classList.remove("invalid");
            capital.classList.add("valid");
        } else {
            capital.classList.remove("valid");
            capital.classList.add("invalid");
        }

        // Validate numbers
        var numbers = /[0-9]/g;
        if (myInput.value.match(numbers)) {
            number.classList.remove("invalid");
            number.classList.add("valid");
        } else {
            number.classList.remove("valid");
            number.classList.add("invalid");
        }

        // Validate length
        if (myInput.value.length >= 8) {
            length.classList.remove("invalid");
            length.classList.add("valid");
        } else {
            length.classList.remove("valid");
            length.classList.add("invalid");
        }
    }

    confirmPassword.onkeyup = function () {
        if (myInput.value == confirmPassword.value) {
            cp.classList.remove("invalid");
            cp.classList.add("valid");
        } else {
            cp.classList.remove("valid");
            cp.classList.add("invalid");
        }
    }

    function checkPasswords() {
        if (myInput.value != confirmPassword.value)
            confirmPassword.setCustomValidity("Паролі не співпадають.");
        else
            confirmPassword.setCustomValidity("");
    }
    function checkUsername() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                if(this.responseText == "true")
                    username.setCustomValidity("");
                else
                    username.setCustomValidity("Це ім'я вже зайнято");
            }
        };
        xhttp.open("POST", "${pageContext.request.contextPath}/checkUsername", true);
        var sendObject = username.value;
        xhttp.send(sendObject);

    }
</script>
</body>
</html>
