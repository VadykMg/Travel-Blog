<%--
  Created by IntelliJ IDEA.
  User: VadykMg
  Date: 29.06.2018
  Time: 20:53
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="../parts/header.jsp"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/styles/card.css">
<body>
<jsp:include page="../parts/navbar.jsp"/>

<div class="card-columns">
    <c:forEach var="article" items="${articlesList}">
        <div class="card" style = "height: 40rem;">
            <div class="card-img-top" style="height: 20rem">
                <c:if test="${article.image != ''}">
                    <img style="height: 20rem;" src="upload/${article.image}" class="card-img-top">
                </c:if>
            </div>
            <div class="card-body" style="height: 15rem;">
                <h3><a href="${pageContext.request.contextPath}/article?id=${article.id}"><c:out value="${article.name} "/></a></h3>
                <div class="card-text">
                <span><c:out value="${article.description} "/></span>
                </div>
            </div>
            <div class="card-footer text-muted" style="height: 5rem;">
                <c:out value="Автор ${article.author.username}. "/>
                <br>
                <c:out value="Тег"/>
                <i><c:out value="${article.tag}."/></i>
            </div>
        </div>
    </c:forEach>
</div>
<jsp:include page="../parts/scripts.jsp"/>
</body>
</html>
