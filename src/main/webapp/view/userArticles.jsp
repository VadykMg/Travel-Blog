<%--
  Created by IntelliJ IDEA.
  User: VadykMg
  Date: 03.07.2018
  Time: 17:39
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="../parts/header.jsp"/>
<body>
<jsp:include page="../parts/navbar.jsp"/>
<table class="table table-sm">
    <thead>
    <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Data</th>
        <th>Tag</th>
        <th>Last update</th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <c:if test="${userArticlesList != null}">
    <c:forEach var="article" items="${userArticlesList}">
        <tr>
            <td>${article.name}</td>
            <td>${article.description}</td>
            <td>${article.date}</td>
            <td>${article.tag}</td>
            <td>${article.lastUpdate}</td>
            <td>
                <form method="get" action="${pageContext.request.contextPath}/protected/editArticle">
                    <input type="hidden" name="a_id" value="${article.id}">
                    <input class="btn btn-primary" type="submit" value="Update">
                </form>
            </td>
            <td>
                <form method="post" action="${pageContext.request.contextPath}/protected/articleDelete">
                    <input type="hidden" name="id" value="${article.id}">
                    <input class="btn btn-danger" type="submit" value="Delete">
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
    </c:if>
    <jsp:include page="../parts/scripts.jsp"/>
</body>
</html>
