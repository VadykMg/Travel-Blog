<%--
  Created by IntelliJ IDEA.
  User: VadykMg
  Date: 04.07.2018
  Time: 12:41
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="../parts/header.jsp"/>
<body>
<jsp:include page="../parts/navbar.jsp"/>
<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <h1 class="mt-4">${article.name}</h1>
            <p class="lead">
                by ${article.author.username}
            <c:if test="${id != null}">
            <c:if test="${username == article.author.username}">
            <form method="get" action="${pageContext.request.contextPath}/protected/editArticle">
                <input type="hidden" name="a_id" value="${article.id}">
                <input class="btn btn-primary" type="submit" value="Update">
            </form>
            </c:if>
            </c:if>
            </p>
            <hr>
            <p>Posted on ${article.lastUpdate}</p>
            <hr>
            <img class="img-fluid rounded" src="${pageContext.request.contextPath}/upload/${article.image}" alt>
            <hr>
            <p class="lead">
                ${article.text}
            </p>
        </div>
    </div>
</div>
<jsp:include page="../parts/scripts.jsp"/>
</body>
</html>
