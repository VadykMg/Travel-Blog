<%--
  Created by IntelliJ IDEA.
  User: VadykMg
  Date: 12.07.2018
  Time: 12:16
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="../parts/header.jsp"/>
<body>
<jsp:include page="../parts/navbar.jsp"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/styles/card.css">
<c:if test="${searchList == null}">
    <h3>Нічого не знайдено.</h3>
</c:if>
<c:if test="${searchList != null}">
    <div class="card-columns">
        <c:forEach var="article" items="${searchList}">
            <div class="card" style="height: 40rem;">
                <div class="card-img-top" style="height: 20rem">
                    <c:if test="${article.image != ''}">
                        <img style="height: 20rem;" src="upload/${article.image}" class="card-img-top">
                    </c:if>
                </div>
                <div class="card-body" style="height: 15rem;">
                    <h3><a href="${pageContext.request.contextPath}/article?id=${article.id}"><c:out value="${article.name} "/></a></h3>
                    <div class="card-text">
                        <span><c:out value="${article.description} "/></span>
                    </div>
                </div>
                <div class="card-footer text-muted" style="height: 5rem;">
                    <c:out value="Автор ${article.author.username}. "/>
                    <br>
                    <c:out value="Тег"/>
                    <i><c:out value="${article.tag}."/></i>
                </div>
            </div>
        </c:forEach>
    </div>
</c:if>
<jsp:include page="../parts/scripts.jsp"/>
</body>
</html>