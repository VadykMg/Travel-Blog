package dao.daoImpl;

import dao.PostDao;
import dao.UserDao;
import entity.Post;
import entity.User;
import util.ConnectionUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PostDaoImpl implements PostDao {

    private static Connection connection = ConnectionUtils.getConnection();
    private UserDao userDao = new UserDaoImpl();

    @Override
    public boolean createPost(String name, String description, String text, String image, User author, boolean available, String tag) throws SQLException {
        String sql = "INSERT INTO public.post(name, description,image,date_upload,author,available,tag, date_update,text) VALUES(?,?,?,?,?,?,?,?,?)";
        boolean isCreated = false;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            Timestamp time = new Timestamp(System.currentTimeMillis());
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, description);
            preparedStatement.setString(3, image);
            preparedStatement.setTimestamp(4, time);
            preparedStatement.setLong(5, author.getId());
            preparedStatement.setBoolean(6, available);
            preparedStatement.setString(7, tag);
            preparedStatement.setTimestamp(8, time);
            preparedStatement.setString(9, text);
            preparedStatement.execute();
            isCreated = true;
        }
        return isCreated;
    }

    @Override
    public boolean updatePost(Long id, String name, String description, String text, boolean available) throws SQLException {
        String sql = "UPDATE public.post SET name = ?, description = ?, available = ?, date_update = ?, text = ? WHERE id = ?";
        boolean isUpdate = false;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, description);
            preparedStatement.setBoolean(3, available);
            preparedStatement.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            preparedStatement.setString(5, text);
            preparedStatement.setLong(6, id);
            preparedStatement.execute();
            isUpdate = true;
        }

        return isUpdate;
    }

    @Override
    public boolean deletePost(Long id) throws SQLException {
        String sql = "DELETE FROM public.post WHERE id  = ?";
        boolean isDelete = false;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            isDelete = true;
        }
        return isDelete;
    }

    @Override
    public Post getPost(Long id) throws SQLException {
        Post post = new Post();
        String sql = "SELECT * FROM public.post WHERE id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.next();
                post.setId(id);
                post.setName(resultSet.getString("name"));
                post.setDescription(resultSet.getString("description"));
                post.setText(resultSet.getString("text"));
                post.setImage(resultSet.getString("image"));
                post.setDate(resultSet.getTimestamp("date_upload"));
                post.setAuthor(userDao.getUserById(resultSet.getLong("author")));
                post.setAvailable(resultSet.getBoolean("available"));
                post.setTag(resultSet.getString("tag"));
                post.setLastUpdate(resultSet.getTimestamp("date_update"));
            }

        }
        return post;
    }

    @Override
    public List<Post> getAllPosts() throws SQLException {
        List<Post> posts = new ArrayList<>();
        String sql = "SELECT * FROM public.post WHERE available = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setBoolean(1, true);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Post post = new Post();
                    post.setId(resultSet.getLong("id"));
                    post.setName(resultSet.getString("name"));
                    post.setDescription(resultSet.getString("description"));
                    post.setText(resultSet.getString("text"));
                    post.setImage(resultSet.getString("image"));
                    post.setDate(resultSet.getTimestamp("date_upload"));
                    post.setAuthor(userDao.getUserById(resultSet.getLong("author")));
                    post.setAvailable(resultSet.getBoolean("available"));
                    post.setTag(resultSet.getString("tag"));
                    post.setLastUpdate(resultSet.getTimestamp("date_update"));
                    posts.add(post);
                }
            }
        }
        return posts;
    }

    @Override
    public List<Post> getAllPostsForId(Long id) throws SQLException {
        List<Post> posts = new ArrayList<>();
        String sql = "SELECT * FROM public.post WHERE author = ? AND available = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            preparedStatement.setBoolean(2, true);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Post post = new Post();
                    post.setId(resultSet.getLong("id"));
                    post.setName(resultSet.getString("name"));
                    post.setDescription(resultSet.getString("description"));
                    post.setText(resultSet.getString("text"));
                    post.setImage(resultSet.getString("image"));
                    post.setDate(resultSet.getTimestamp("date_upload"));
                    post.setAuthor(userDao.getUserById(resultSet.getLong("author")));
                    post.setAvailable(resultSet.getBoolean("available"));
                    post.setTag(resultSet.getString("tag"));
                    post.setLastUpdate(resultSet.getTimestamp("date_update"));
                    posts.add(post);
                }
            }
        }
        return posts;
    }

    @Override
    public List<Post> getAllUnavailable(Long id) throws SQLException {
        List<Post> posts = new ArrayList<>();
        String sql = "SELECT * FROM public.post WHERE author = ? AND available = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            preparedStatement.setBoolean(2, false);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Post post = new Post();
                    post.setId(resultSet.getLong("id"));
                    post.setName(resultSet.getString("name"));
                    post.setDescription(resultSet.getString("description"));
                    post.setText(resultSet.getString("text"));
                    post.setImage(resultSet.getString("image"));
                    post.setDate(resultSet.getTimestamp("date_upload"));
                    post.setAuthor(userDao.getUserById(resultSet.getLong("author")));
                    post.setAvailable(resultSet.getBoolean("available"));
                    post.setTag(resultSet.getString("tag"));
                    post.setLastUpdate(resultSet.getTimestamp("date_update"));
                    posts.add(post);
                }
            }
        }
        return posts;
    }

    @Override
    public boolean deleteAllPostOfUser(Long id) throws SQLException {
        String sql = "DELETE FROM public.post WHERE author  = ?";
        boolean isDelete = false;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            isDelete = true;
        }
        return isDelete;
    }

    @Override
    public List<Post> getAllBuyTag(String tag) throws SQLException {
        List<Post> posts = new ArrayList<>();
        String sql = "SELECT * FROM public.post WHERE tag = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, tag);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Post post = new Post();
                    post.setId(resultSet.getLong("id"));
                    post.setName(resultSet.getString("name"));
                    post.setDescription(resultSet.getString("description"));
                    post.setText(resultSet.getString("text"));
                    post.setImage(resultSet.getString("image"));
                    post.setDate(resultSet.getTimestamp("date_upload"));
                    post.setAuthor(userDao.getUserById(resultSet.getLong("author")));
                    post.setAvailable(resultSet.getBoolean("available"));
                    post.setTag(resultSet.getString("tag"));
                    post.setLastUpdate(resultSet.getTimestamp("date_update"));
                    posts.add(post);
                    System.out.println(post.getName());
                }
            }
        }
        return posts;
    }

}

