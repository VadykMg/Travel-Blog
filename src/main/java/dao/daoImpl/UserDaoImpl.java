package dao.daoImpl;

import dao.UserDao;
import entity.Role;
import entity.User;
import util.ConnectionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserDaoImpl implements UserDao {

    private Connection connection = ConnectionUtils.getConnection();

    @Override
    public boolean saveUser(String username, String password) throws SQLException {
        boolean isSave = false;
        if (getUserByUsername(username) == null) {
            String sql = "INSERT INTO public.user(password, username, is_baned) values (?,?,?) RETURNING id";
            Long id;
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

                preparedStatement.setString(1, password);
                preparedStatement.setString(2, username);
                preparedStatement.setBoolean(3,false);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    resultSet.next();
                    id = resultSet.getLong("id");
                }
                isSave = true;
            }
            Set<Role> roles = new HashSet<>();
            roles.add(Role.USER);
            addUserRole(id, roles);
        }
        return isSave;
    }

    @Override
    public boolean updateUser(Long id, String password) throws SQLException {
        String sql = "UPDATE public.user SET  password = ?  WHERE id = ?";
        boolean isUpdate = false;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setString(1, password);
            preparedStatement.setLong(2, id);
            preparedStatement.execute();
            isUpdate = true;
        }
        return isUpdate;
    }

    @Override
    public boolean deleteUser(Long id) throws SQLException {
        String sql = "DELETE FROM public.user WHERE id = ?";
        String sqlRole = "DELETE FROM public.user_role WHERE user_id = ?";
        boolean isDelete = false;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlRole)) {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        new PostDaoImpl().deleteAllPostOfUser(id);
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            isDelete = true;
        }
        return isDelete;
    }

    @Override
    public User getUserById(Long id) throws SQLException {
        String sql = "SELECT * FROM public.user WHERE id = ?";
        User user = new User();
        user.setId(id);
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.next();
                user.setUsername(resultSet.getString(3));
                user.setPassword(resultSet.getString(2));
                user.setBaned(resultSet.getBoolean(4));
                user.setRole(getUserRole(id));
            }
        }
        return user;
    }

    @Override
    public User getUserByUsername(String username) throws SQLException {
        String sql = "SELECT * FROM public.user WHERE username = ?";
        User user = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, username);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    user = new User();
                    user.setId(resultSet.getLong(1));
                    user.setPassword(resultSet.getString(2));
                    user.setUsername(username);
                    user.setBaned(resultSet.getBoolean(4));
                }
            }
        }
        return user;
    }

    @Override
    public Set<Role> getUserRole(Long id) throws SQLException {
        String sql = "SELECT * FROM public.user_role WHERE user_id = ?";
        Set<Role> roles = new HashSet<>();
        if (id != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setLong(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        roles.add(Role.valueOf(resultSet.getString("roles")));
                    }
                }
            }
            return roles;
        }
        return null;
    }

    @Override
    public boolean addUserRole(Long id, Set<Role> roles) throws SQLException {
        Set<Role> userRoles = roles;

        String sql = "DELETE FROM public.user_role WHERE user_id = ?";
        boolean isUserRoleAdd = false;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
        }
        for (Role role : userRoles) {
            sql = "INSERT INTO public.user_role(user_id, roles) VALUES (?,?)";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setLong(1, id);
                preparedStatement.setString(2, role.toString());
                preparedStatement.execute();
                isUserRoleAdd = true;
            }
        }
        return isUserRoleAdd;
    }

    @Override
    public List<User> getAllUsers() throws SQLException {
        String sql = "SELECT * FROM public.user";
        List<User> users = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    User user = new User();
                    user.setId(resultSet.getLong("id"));
                    user.setUsername(resultSet.getString("username"));
                    user.setPassword(resultSet.getString("password"));
                    user.setRole(getUserRole(user.getId()));
                    user.setBaned(resultSet.getBoolean("is_baned"));
                    users.add(user);
                }
            }
        }
        return users;
    }

    @Override
    public User getUserByUsernameAndPassword(String username, String password) throws SQLException {

        String sql = "SELECT * FROM public.user WHERE username = ? AND password = ?";
        User user;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    user = new User();
                    user.setBaned(resultSet.getBoolean("is_baned"));
                    user.setUsername(resultSet.getString(3));
                    user.setPassword(resultSet.getString(2));
                    user.setId(resultSet.getLong(1));
                    return user;
                }
            }
        }
        return null;
    }

    @Override
    public void banUser(String username) throws SQLException {
        String sql = "UPDATE public.user SET is_baned = ? WHERE username = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setBoolean(1,true);
            preparedStatement.setString(2,username);
            System.out.println(username);
            preparedStatement.execute();
        }
    }

    @Override
    public void unBanUser(String username) throws SQLException {
        String sql = "UPDATE public.user SET is_baned = ? WHERE username = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setBoolean(1,false);
            preparedStatement.setString(2,username);
            preparedStatement.execute();
        }
    }
}

