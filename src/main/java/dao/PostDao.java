package dao;

import entity.Post;
import entity.User;
import javafx.geometry.Pos;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public interface PostDao {
    boolean createPost(String name, String description, String text, String image, User author, boolean available, String tag) throws SQLException;

    boolean updatePost(Long id, String name, String description, String text, boolean available) throws SQLException;

    boolean deletePost(Long id) throws SQLException;

    Post getPost(Long id) throws SQLException;

    List<Post> getAllPosts() throws SQLException;

    List<Post> getAllPostsForId(Long id) throws SQLException;

    List<Post> getAllUnavailable(Long id) throws SQLException;

    boolean deleteAllPostOfUser(Long id) throws SQLException;

    List<Post> getAllBuyTag(String tag) throws SQLException;
}

