package dao;

import entity.Role;
import entity.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

public interface UserDao {
    boolean saveUser(String username, String password) throws SQLException;

    boolean updateUser(Long id, String password) throws SQLException;

    boolean deleteUser(Long id) throws SQLException;

    User getUserById(Long id) throws SQLException;

    User getUserByUsername(String username) throws SQLException;

    Set<Role> getUserRole(Long id) throws SQLException;

    boolean addUserRole(Long id, Set<Role> roles) throws SQLException;

    List<User> getAllUsers() throws SQLException;

    User getUserByUsernameAndPassword(String username, String password) throws SQLException;
    void banUser(String username) throws SQLException;
    void unBanUser(String username) throws SQLException;
}


