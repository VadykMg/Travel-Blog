package entity;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;

public class Post {
    @NotNull
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String description;
    @NotNull
    private String text;
    private String image;
    @NotNull
    private Timestamp date;
    @NotNull
    private User author;
    @NotNull
    private boolean available;
    @NotNull
    private String tag;
    @NotNull
    private Timestamp lastUpdate;

    public Post() {

    }

    public Post(String name, String description,String text, String image, Timestamp date, User author, boolean available, String tag) {
        this.name = name;
        this.description = description;
        this.image = image;
        this.date = date;
        this.author = author;
        this.available = available;
        this.tag = tag;
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Post post = (Post) o;

        if (available != post.available) return false;
        if (id != null ? !id.equals(post.id) : post.id != null) return false;
        if (name != null ? !name.equals(post.name) : post.name != null) return false;
        if (description != null ? !description.equals(post.description) : post.description != null) return false;
        if (image != null ? !image.equals(post.image) : post.image != null) return false;
        if (date != null ? !date.equals(post.date) : post.date != null) return false;
        if (author != null ? !author.equals(post.author) : post.author != null) return false;
        if (tag != null ? !tag.equals(post.tag) : post.tag != null) return false;
        return lastUpdate != null ? lastUpdate.equals(post.lastUpdate) : post.lastUpdate == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (available ? 1 : 0);
        result = 31 * result + (tag != null ? tag.hashCode() : 0);
        result = 31 * result + (lastUpdate != null ? lastUpdate.hashCode() : 0);
        return result;
    }
}

