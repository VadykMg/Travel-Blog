package util;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesUtils {
    private static final String FILE_NAME = PropertiesUtils.class.getClassLoader().getResource("db.properties").getPath();

    private PropertiesUtils() {

    }

    public static Properties getProperties() {
        Properties properties = new Properties();
        try (FileInputStream fileInputStream = new FileInputStream(FILE_NAME)) {
            properties.load(fileInputStream);
        } catch (IOException e) {
            ExceptionHandler.log("Неможливо прочитати файл конфігурації", PropertiesUtils.class);
        }
        return properties;
    }
}

