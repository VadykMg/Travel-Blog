package util;

import entity.Post;
import entity.User;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class ValidatorHandler {

    private ValidatorHandler() {

    }

    public static Set<ConstraintViolation<User>> validateUser(User user) {
        ValidatorFactory validatorFactory =  Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        return validator.validate(user);
    }


    public static Set<ConstraintViolation<Post>> validatePost(Post post) {
        ValidatorFactory validatorFactory =  Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        return validator.validate(post);
    }

    public static Validator getValidator() {
        ValidatorFactory validatorFactory =  Validation.buildDefaultValidatorFactory();
        return validatorFactory.getValidator();
    }
}
