package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtils {
    private static final String DB_URL = PropertiesUtils.getProperties().getProperty("DB_URL");
    private static final String USER = PropertiesUtils.getProperties().getProperty("USER");
    private static final String PASSWORD = PropertiesUtils.getProperties().getProperty("PASSWORD");

    private ConnectionUtils() {

    }

    public static Connection getConnection() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            ExceptionHandler.log("Неможливо знайти драйвер", ConnectionUtils.class);
        }
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо встановити з'єднання з БД", ConnectionUtils.class);
        }
        if (connection != null) {
            return connection;
        }
        throw new NullPointerException();
    }
}

