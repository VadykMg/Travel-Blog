package util;

import org.apache.log4j.Logger;

public class ExceptionHandler {
    private ExceptionHandler(){

    }
    public static void log(String msg, Class clazz) {
        Logger log = Logger.getLogger(clazz);
        log.error(msg);
    }
}
