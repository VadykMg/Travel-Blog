package service;

import dao.PostDao;
import dao.daoImpl.PostDaoImpl;
import entity.Post;
import entity.User;
import javafx.geometry.Pos;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public class PostService {
    private PostDao postDao = new PostDaoImpl();

    public List<Post> getAllPosts() throws SQLException {
        List<Post> posts = postDao.getAllPosts();
        posts.sort((o1, o2) -> o2.getLastUpdate().compareTo(o1.getLastUpdate()));
        return posts;
    }

    public void createPost(Post post) throws SQLException {
        postDao.createPost(post.getName(), post.getDescription(), post.getText(), post.getImage(), post.getAuthor(), post.isAvailable(), post.getTag());
    }

    public List<Post> getAllPostsForId(Long id) throws SQLException {
        return postDao.getAllPostsForId(id);
    }

    public List<Post> getAllUnavailable(Long id) throws SQLException {
        return postDao.getAllUnavailable(id);
    }

    public void deletePost(Long id) throws SQLException {
        postDao.deletePost(id);
    }

    public void updatePost(Long id, String name, String text, String description, boolean available) throws SQLException {
        postDao.updatePost(id, name, description, text, available);
    }

    public Post getPost(Long id) throws SQLException {
        return postDao.getPost(id);
    }

    public List<Post> getAllByTag(String tag) throws SQLException {
        return postDao.getAllBuyTag(tag);
    }
}
