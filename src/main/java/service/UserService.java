package service;

import dao.UserDao;
import dao.daoImpl.UserDaoImpl;
import entity.Role;
import entity.User;
import org.apache.commons.codec.digest.DigestUtils;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

public class UserService {
    private UserDao userDao = new UserDaoImpl();

    public boolean saveUser(String username, String password) throws SQLException {
        return userDao.saveUser(username, DigestUtils.md5Hex(password));
    }

    public Set<Role> getUserRole(Long id) throws SQLException {
        return userDao.getUserRole(id);
    }

    public User getUserByUsername(String username) throws SQLException {
        return userDao.getUserByUsername(username);
    }

    public boolean userIsExist(String username, String password) throws SQLException {
        if (username == null || password == null)
            return false;
        return userDao.getUserByUsernameAndPassword(username, DigestUtils.md5Hex(password)) != null;
    }

    public User getUserByUsernameAndPassword(String username, String password) throws SQLException {
        if (username == null || password == null)
            return null;
        return userDao.getUserByUsernameAndPassword(username, DigestUtils.md5Hex(password));
    }

    public List<User> getAllUsers() throws SQLException {
        return userDao.getAllUsers();
    }

    public User getUserById(Long id) throws SQLException {
        return userDao.getUserById(id);
    }

    public void updateUser(Long id, String password) throws SQLException {
        userDao.updateUser(id, DigestUtils.md5Hex(password));
    }

    public void deleteUser(Long id) throws SQLException {
        userDao.deleteUser(id);
    }


    public void banUser(String username) throws SQLException {
        userDao.banUser(username);
    }

    public void unBanUser(String username) throws SQLException {
        userDao.unBanUser(username);
    }
    public void setRoles(Long id, Set<Role> roles) throws SQLException {
        userDao.addUserRole(id, roles);
    }
}
