package controller.auth;

import controller.article.UpdateArticle;
import entity.Post;
import entity.User;
import service.UserService;
import util.ExceptionHandler;
import util.ValidatorHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public class RegistrationController extends HttpServlet {
    private static final AtomicReference<UserService> userService = new AtomicReference<>(new UserService());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/view/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        boolean isSave = false;
        User user = new User();
        user.setUsername(req.getParameter("username"));
        user.setPassword(req.getParameter("password"));
        Set<ConstraintViolation<User>> constraintViolations = ValidatorHandler.getValidator().validateProperty(user, "username");
        constraintViolations.addAll(ValidatorHandler.getValidator().validateProperty(user, "password"));
        if(constraintViolations.isEmpty()) {
        try {
            isSave = userService.get().saveUser(user.getUsername(), user.getPassword());
        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо зберегти користувача", RegistrationController.class);
        }
        if (isSave) {
            resp.sendRedirect(req.getContextPath()+"/protected/");
        }
        } else {
            List<String> messages =  new ArrayList<>();
            for(ConstraintViolation<User> violation : constraintViolations) {
                messages.add(violation.getMessage());
            }
            req.setAttribute("messages", messages);
            req.getRequestDispatcher("/view/error.jsp").forward(req,resp);
        }

    }
}
