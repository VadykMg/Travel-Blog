package controller.auth.filter;

import controller.article.AddArticleController;
import entity.Role;
import org.apache.log4j.Logger;
import service.UserService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.Objects.nonNull;

public class AuthFilter implements Filter {
    private static final Logger log = Logger.getLogger(AuthFilter.class);
    private String encoding;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        encoding = filterConfig.getInitParameter("requestEncoding");
        if (encoding == null) encoding = "UTF-8";
    }

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        if (null == request.getCharacterEncoding()) {
            request.setCharacterEncoding(encoding);
        }
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;
        final String login = req.getParameter("username");
        final String password = req.getParameter("password");
        final AtomicReference<UserService> userService = new AtomicReference<>(new UserService());

        final HttpSession session = req.getSession();

        //Logged user.
        if (nonNull(session) &&
                nonNull(session.getAttribute("id")) &&
                nonNull(session.getAttribute("role"))) {
            chain.doFilter(request, response);

        } else {
            try {
                if (userService.get().userIsExist(login, password)) {
                    final Set<Role> roles = userService.get().getUserRole(userService.get().getUserByUsernameAndPassword(login, password).getId());
                    req.getSession().setAttribute("role", roles);
                    req.getSession().setAttribute("username", userService.get().getUserByUsername(login).getUsername());
                    req.getSession().setAttribute("id", userService.get().getUserByUsername(login).getId());
                    moveToMenu(req, res, roles);

                } else {
                    moveToMenu(req, res, null);
                }
            } catch (SQLException e) {
                log.error("Неможливо отримати користувача при аутефікації");
            }
        }
    }

    private void moveToMenu(HttpServletRequest req, HttpServletResponse res, Set<Role> roles) throws ServletException, IOException {

        if (roles != null && roles.contains(Role.ADMIN)) {
            req.getSession().setAttribute("role", Role.ADMIN.toString());
                        res.sendRedirect(req.getContextPath()+"/");
        } else if (roles != null && roles.contains(Role.USER)) {
            req.getSession().setAttribute("role", Role.USER.toString());
            res.sendRedirect(req.getContextPath()+"/");
        } else {
            req.getRequestDispatcher("/view/login.jsp").forward(req, res);
        }
    }

    @Override
    public void destroy() throws UnsupportedOperationException {

    }
}
