package controller.user;

import controller.article.UpdateArticle;
import entity.User;
import service.UserService;
import util.ExceptionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

public class CheckBanUserController extends HttpServlet {private final AtomicReference<UserService> userService = new AtomicReference<>(new UserService());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BufferedReader reader = req.getReader();
        String username = reader.readLine();
        User userByUsername;
        try {
            userByUsername = userService.get().getUserByUsername(username);
            if(userByUsername.getIsBaned())
                resp.getWriter().write("true");
            else
                resp.getWriter().write("false");
        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо перевірити, чи користувач забанений", CheckBanUserController.class);
        }
    }
}
