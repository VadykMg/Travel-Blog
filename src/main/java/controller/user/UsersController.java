package controller.user;

import service.UserService;
import util.ExceptionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

public class UsersController extends HttpServlet {
    private static final AtomicReference<UserService> userService = new AtomicReference<>(new UserService());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.setAttribute("usersList", userService.get().getAllUsers());
        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо отримати список користувачів", UsersController.class);;
        }
        req.getRequestDispatcher("/view/listOfUsers.jsp").forward(req, resp);
    }
}
