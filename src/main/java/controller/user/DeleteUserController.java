package controller.user;

import service.UserService;
import util.ExceptionHandler;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

public class DeleteUserController extends HttpServlet {
    private static final AtomicReference<UserService> userService = new AtomicReference<>(new UserService());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long id = Long.valueOf(String.valueOf(req.getParameter("id")));
        try {
            userService.get().deleteUser(id);
        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо видалити користувача", DeleteUserController.class);
        }
        resp.sendRedirect(req.getContextPath()+"/protected/users");
    }
}
