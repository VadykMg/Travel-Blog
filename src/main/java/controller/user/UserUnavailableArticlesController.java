package controller.user;

import entity.Post;
import org.apache.log4j.Logger;
import service.PostService;
import util.ExceptionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class UserUnavailableArticlesController extends HttpServlet {
    private static final AtomicReference<PostService> postService = new AtomicReference<>(new PostService());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(String.valueOf(req.getSession().getAttribute("id")));
        List<Post> posts = null;
        try {
            posts = postService.get().getAllUnavailable(id);
        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо отримати чернетки", UserUnavailableArticlesController.class);
        }
        req.setAttribute("userArticlesList", posts);
        req.getRequestDispatcher("/view/userArticles.jsp").forward(req, resp);
    }
}
