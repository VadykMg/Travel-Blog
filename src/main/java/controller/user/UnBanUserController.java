package controller.user;

import service.UserService;
import util.ExceptionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

public class UnBanUserController extends HttpServlet {
    private final AtomicReference<UserService> userService = new AtomicReference<>(new UserService());
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        try {
            userService.get().unBanUser(username);
        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо розбанити користувача", UnBanUserController.class);
        }
        resp.sendRedirect(req.getContextPath()+"/protected/users");
    }
}
