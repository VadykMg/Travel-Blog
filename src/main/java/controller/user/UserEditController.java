package controller.user;

import entity.Role;
import entity.User;
import service.UserService;
import util.ExceptionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public class UserEditController extends HttpServlet {
    private static final AtomicReference<UserService> userService = new AtomicReference<>(new UserService());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("id"));
        User user = null;
        try {
            user = userService.get().getUserById(id);
        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо отримати користувача", UserEditController.class);
        }
        req.setAttribute("user", user);
        req.getRequestDispatcher("/view/userEdit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("id"));
        if (req.getParameter("USER") == null && req.getParameter("ADMIN") == null) {
            try {
                if (!req.getParameter("password").equals(""))
                    userService.get().updateUser(id, req.getParameter("password"));
            } catch (SQLException e) {
                ExceptionHandler.log("Неможливо оновити користувача", UserEditController.class);
            }
        } else {
            Set<Role> roles = new HashSet<>();
            if (req.getParameter("USER") != null && req.getParameter("USER").equals("on")) {
                roles.add(Role.USER);
            }
            if (req.getParameter("ADMIN") != null && req.getParameter("ADMIN").equals("on")) {
                roles.add(Role.ADMIN);
            }
            if (!roles.isEmpty()) {
                try {
                    userService.get().setRoles(id, roles);
                } catch (SQLException e) {
                    ExceptionHandler.log("Неможливо поміняти роль користувача", UserEditController.class);
                }
            }
            if (!req.getParameter("password").equals("")) {
                try {
                    userService.get().updateUser(id, req.getParameter("password"));
                } catch (SQLException e) {
                    ExceptionHandler.log("Неможливо оновити користувача", UserEditController.class);
                }
            }

        }
        if (req.getParameter("role").equals("ADMIN"))
            resp.sendRedirect(req.getContextPath()+"/protected/users");
        else
            req.getRequestDispatcher("/view/userEdit.jsp").forward(req, resp);
    }
}
