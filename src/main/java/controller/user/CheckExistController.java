package controller.user;

import entity.User;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import service.UserService;
import util.ExceptionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

public class CheckExistController extends HttpServlet {
    private static final AtomicReference<UserService> userService = new AtomicReference<>(new UserService());
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Object obj = new JSONParser().parse(req.getReader());
            JSONObject jo = (JSONObject) obj;
            String username = (String) jo.get("username");
            String password = (String) jo.get("password");
            User userByUsernameAndPassword = userService.get().getUserByUsernameAndPassword(username, password);
            if(userByUsernameAndPassword == null) {
                resp.getWriter().write("false");
            } else {
                resp.getWriter().write("true");
            }
        } catch (ParseException | SQLException e) {
            ExceptionHandler.log("Неможливо перевірити юзера", CheckExistController.class);
        }
    }
}
