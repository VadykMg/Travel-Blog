package controller.article;

import entity.Post;
import org.apache.log4j.Logger;
import service.PostService;
import util.ExceptionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public class SearchController extends HttpServlet {
    private static AtomicReference<PostService> postService = new AtomicReference<>(new PostService());
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String tag = req.getParameter("tag");
        Set<Post> sPosts = new HashSet<>();
        try {
            List<Post> posts =  postService.get().getAllPosts();
            for(Post post: posts) {
                if(post.getName().toLowerCase().contains(tag.toLowerCase())) {
                    sPosts.add(post);
                }
            }

            if(!sPosts.isEmpty())
                req.setAttribute("searchList", sPosts);
            else
                req.setAttribute("searchList", null);

        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо знайти статтю", SearchController.class);
        }

        req.getRequestDispatcher("view/search.jsp").forward(req,resp);
    }
}
