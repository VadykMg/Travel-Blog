package controller.article;

import entity.Post;
import org.apache.log4j.Logger;
import service.PostService;
import util.ExceptionHandler;
import util.ValidatorHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public class UpdateArticle extends HttpServlet {
    private static final AtomicReference<PostService> postService = new AtomicReference<>(new PostService());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("a_id"));
        Post post = null;
        try {
            post = postService.get().getPost(id);
        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо отримати статтю для редагування", UpdateArticle.class);
        }
        req.setAttribute("article", post);
        req.getRequestDispatcher("/view/editArticle.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("id"));
        String name = req.getParameter("name");
        String description = req.getParameter("description");
        String text = req.getParameter("text");

        Post post = new Post();
        post.setId(id);
        post.setName(name);
        post.setDescription(description);
        post.setText(text);
        Set<ConstraintViolation<Post>> validatePost = ValidatorHandler.getValidator().validateProperty(post, "id");
        validatePost.addAll(ValidatorHandler.getValidator().validateProperty(post, "name"));
        validatePost.addAll(ValidatorHandler.getValidator().validateProperty(post, "text"));
        validatePost.addAll(ValidatorHandler.getValidator().validateProperty(post, "description"));

        if(validatePost.isEmpty()) {
            boolean available = false;
            if (req.getParameter("available") != null && req.getParameter("available").equals("on"))
                available = true;
            try {
                postService.get().updatePost(id, name, text, description, available);
            } catch (SQLException e) {
                ExceptionHandler.log("Неможливо оновити статтю", UpdateArticle.class);
            }
            resp.sendRedirect(req.getContextPath()+"/protected/userArticles");
        } else {
            List<String> messages =  new ArrayList<>();
            for(ConstraintViolation<Post> constraintViolation : validatePost) {
                messages.add(constraintViolation.getMessage());
            }
            req.setAttribute("messages", messages);
            req.getRequestDispatcher("/view/error.jsp").forward(req,resp);
        }
    }
}
