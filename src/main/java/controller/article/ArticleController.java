package controller.article;

import entity.Post;
import org.apache.log4j.Logger;
import service.PostService;
import util.ExceptionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

public class ArticleController extends HttpServlet {
    private static final AtomicReference<PostService> postService = new AtomicReference<>(new PostService());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("id"));
        Post post = null;
        try {
            post = postService.get().getPost(id);
        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо отримати статтю з БД", ArticleController.class);
        }
        req.setAttribute("article", post);
        req.getRequestDispatcher("/view/article.jsp").forward(req, resp);
    }
}
