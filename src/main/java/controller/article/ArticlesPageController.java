package controller.article;

import org.apache.log4j.Logger;
import service.PostService;
import util.ExceptionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

public class ArticlesPageController extends HttpServlet {
    private static final AtomicReference<PostService> postService = new AtomicReference<>(new PostService());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.setAttribute("articlesList", postService.get().getAllPosts());
        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо отримати список статтей", ArticlesPageController.class);
        }
        req.getRequestDispatcher("/view/articles.jsp").forward(req, resp);
    }
}
