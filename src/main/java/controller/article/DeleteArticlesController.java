package controller.article;

import service.PostService;
import util.ExceptionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

public class DeleteArticlesController extends HttpServlet {
    private static final AtomicReference<PostService> postService = new AtomicReference<>(new PostService());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("id"));
        try {
            postService.get().deletePost(id);
        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо видалити статтю", ArticlesPageController.class);
        }
        resp.sendRedirect(req.getContextPath()+"/protected/userArticles");
    }
}
