package controller.article;

import entity.Post;
import entity.User;
import service.PostService;
import service.UserService;
import util.ExceptionHandler;
import util.ValidatorHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.validation.ConstraintViolation;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

@MultipartConfig
public class AddArticleController extends HttpServlet {
    private String UPLOAD_PATH;
    private static final AtomicReference<PostService> postService = new AtomicReference<>(new PostService());
    private static final AtomicReference<UserService> userService = new AtomicReference<>(new UserService());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/view/addArticle.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UPLOAD_PATH = req.getServletContext().getRealPath("/upload");
        String name = req.getParameter("name");
        String description = req.getParameter("description");
        String text = req.getParameter("text");
        boolean available;
        if (req.getParameter("available") == null)
            available = false;
        else
            available = req.getParameter("available").equals("on");
        String tag = req.getParameter("tag");
        Part filePart = req.getPart("image");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        String resultFile = "balloon.jpg";
        if (!fileName.equals("")) {
            File uploadDir = new File(UPLOAD_PATH);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }

            String uuidFile = UUID.randomUUID().toString();
            resultFile = uuidFile + "." + fileName;
            String image = UPLOAD_PATH + "\\" + resultFile;
            try (InputStream inputStream = filePart.getInputStream()) {
                byte[] buffer = new byte[inputStream.available()];
                try (FileOutputStream fileOutputStream = new FileOutputStream(image)) {
                    inputStream.read(buffer);
                    fileOutputStream.write(buffer);
                }
            }
        }
        User author = null;
        try {
            author = userService.get().getUserById(Long.valueOf(req.getParameter("id")));
        } catch (SQLException e) {
            ExceptionHandler.log("При створенні неможливо дізнатися автора статті", AddArticleController.class);
        }
        try {
            Post post = new Post();
            post.setName(name);
            post.setDescription(description);
            post.setText(text);
            post.setImage(resultFile);
            post.setAuthor(author);
            post.setAvailable(available);
            post.setTag(tag);
            Set<ConstraintViolation<Post>> validatePost = ValidatorHandler.getValidator().validateProperty(post, "name");
            validatePost.addAll(ValidatorHandler.getValidator().validateProperty(post, "description"));
            validatePost.addAll(ValidatorHandler.getValidator().validateProperty(post, "text"));
            validatePost.addAll(ValidatorHandler.getValidator().validateProperty(post, "image"));
            validatePost.addAll(ValidatorHandler.getValidator().validateProperty(post, "author"));
            validatePost.addAll(ValidatorHandler.getValidator().validateProperty(post, "available"));
            validatePost.addAll(ValidatorHandler.getValidator().validateProperty(post, "tag"));

            if(validatePost.isEmpty()) {
                postService.get().createPost(post);
                resp.sendRedirect(req.getContextPath()+"/protected/userArticles");
            } else {
                List<String> messages =  new ArrayList<>();
                for(ConstraintViolation<Post> constraintViolation : validatePost) {
                    messages.add(constraintViolation.getMessage());
                }
                req.setAttribute("messages", messages);
                req.getRequestDispatcher("/view/error.jsp").forward(req,resp);
            }
        } catch (SQLException e) {
            ExceptionHandler.log("Неможливо створити статтю", AddArticleController.class);
        }
    }
}
